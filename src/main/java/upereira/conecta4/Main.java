package upereira.conecta4;

import java.util.Random;
import javax.swing.table.DefaultTableModel;
import upereira.conecta4.algoritmos.AgenteRF;
import upereira.conecta4.algoritmos.MiniMaxAlfaBeta;
import upereira.conecta4.gui.Estadisticas;
import upereira.conecta4.gui.MenuPrincipal;
import upereira.conecta4.juego.Juego;
import upereira.conecta4.juego.JuegoAcciones;
import upereira.conecta4.juego.JuegoEstado;
import upereira.conecta4.juego.Jugador;

public class Main {

    public static void main(String[] args)
    {
        new MenuPrincipal().setVisible(true);     
        
        /*
        int c = 0;
        Jugador[] players = new Jugador[11];
        Jugador[] oponents = new Jugador[11];
        
        for (int i = 2; i <= 6; i++)
        {
            players[c] = new MiniMaxAlfaBeta(false, i, 0);
            oponents[c++] = new MiniMaxAlfaBeta(false, i, 1);
        }
        for (int i = 2; i <= 6; i++)
        {
            players[c] = new MiniMaxAlfaBeta(true, i, 0);
            oponents[c++] = new MiniMaxAlfaBeta(true, i, 1);
        }
        players[c] = new AgenteRF(0);
        ((AgenteRF)players[c]).setTabla(MenuPrincipal.tablaRF);
        oponents[c] = new AgenteRF(1);
        ((AgenteRF)oponents[c]).setTabla(MenuPrincipal.tablaRF);
        
        
        DefaultTableModel model = MenuPrincipal.model;
        Estadisticas estadisticas = new Estadisticas();
        Random r = new Random(System.currentTimeMillis());
        Juego juego = new Juego();
        
        Jugador[] jugadores = new Jugador[2];
        for (int i = 0; i < players.length; i++)
        {
            for (int j = 0; j < 100; j++)
            {
                
                jugadores[0] = players[i];
                jugadores[1] = oponents[r.nextInt(oponents.length)];
                
                while (!juego.isTerminal())
                {
                    Jugador jug = jugadores[juego.getJugadorActual()];
                    JuegoAcciones accion = jug.actuar(juego.getState(), juego.getReward());
                    juego.transition(accion);
                }
                
                // Actualizar Estadisticas
                int ganador = -1;
                    if (!juego.isEmpate()) ganador = (juego.getJugadorActual() + 1) % 2;
                    
                    String algoritmo_1 = null;
                    String algoritmo_2 = null;                                       
                    Long tiempo_1 = null;
                    Long tiempo_2 = null;
                    Estadisticas.TipoEstadistica tipo1 = null;
                    Estadisticas.TipoEstadistica tipo2 = null;
                    
                    if (jugadores[0] != null) algoritmo_1 = jugadores[0].getNombre();
                    if (jugadores[1] != null) algoritmo_2 = jugadores[1].getNombre();                    
                    if (algoritmo_1 != null) tiempo_1 = jugadores[0].getTiempoPromedio();
                    if (algoritmo_2 != null) tiempo_2 = jugadores[1].getTiempoPromedio();
                    if (algoritmo_1 != null) {
                        if (ganador == -1) tipo1 = Estadisticas.TipoEstadistica.EMPATE;
                        else if (ganador == 0) tipo1 = Estadisticas.TipoEstadistica.VICTORIA;
                        else tipo1 = Estadisticas.TipoEstadistica.DERROTA;
                    }
                    if (algoritmo_2 != null) {
                        if (ganador == -1) tipo2 = Estadisticas.TipoEstadistica.EMPATE;
                        else if (ganador == 1) tipo2 = Estadisticas.TipoEstadistica.VICTORIA;
                        else tipo2 = Estadisticas.TipoEstadistica.DERROTA;
                    }
                    
                    if (algoritmo_1 != null)
                    {
                        estadisticas.addEstadistica(algoritmo_1, tipo1, null);
                        estadisticas.addEstadistica(algoritmo_1, Estadisticas.TipoEstadistica.TIEMPO, tiempo_1);
                    }
                    if (algoritmo_2 != null)
                    {
                        estadisticas.addEstadistica(algoritmo_2, tipo2, null);
                        estadisticas.addEstadistica(algoritmo_2, Estadisticas.TipoEstadistica.TIEMPO, tiempo_2);
                    }
                    
                    
                    
                    model.setRowCount(0);


                    Object[][] o = estadisticas.obtenerEstadisticas();
                    for (Object[] row : o)
                    {
                        model.addRow(row);
                    }
                    
                    juego.reiniciar();
            }
        }*/
    }
    
    public static void imprimirTablero(JuegoEstado estado)
    {
        System.out.println("================================");
        for(int i = 0; i < JuegoEstado.FILAS; i++)
        {            
            for(int j = 0; j < JuegoEstado.COLUMNAS; j++)
            {
                System.out.print(estado.matriz[i][j] == -1 ? "_" : estado.matriz[i][j]);
            }
            System.out.print("\n");
        }
        System.out.println("================================");
    }
}
