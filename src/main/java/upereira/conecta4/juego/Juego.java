package upereira.conecta4.juego;

import upereira.utilities.ia.World;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Juego extends World<JuegoEstado, JuegoAcciones> {
    
    public static final int VACIO = -1;
    
    private int turnoJugador;
    private boolean isEmpate;
    private boolean isTerminado;
    private int[] columnas;
    
    private Random random;
    
    public Juego()
    {
        estado = new JuegoEstado(); 
        random = new Random(System.currentTimeMillis());
        
        reiniciar();
        
        actions = new ArrayList<>();        
        actions.addAll(Arrays.asList(JuegoAcciones.values()));
    }
    
    public boolean isEmpate() {
        return isEmpate;
    }
    
    @Override
    public boolean isTerminal() {       
        
        return isTerminado;
    }

    @Override
    public double getReward() {       
        
        if (isTerminado) {
            if (isEmpate) return 0.5;
            else return 1.0;
        }
        return 0;
    }

    @Override
    public void transition(JuegoAcciones action) {
        
        if (action == null) return;
        if (!isTerminado) {           
            int columna = action.getValue();
            if (columnas[columna] < 6)
            {
                columnas[columna]++;
                estado.matriz[JuegoEstado.FILAS - columnas[columna]][columna] = turnoJugador;                
                
                turnoJugador = (turnoJugador + 1) % 2;                
                double r[][] = evaluar(estado);
                
                if (r[2][0] != -1) isTerminado = true;
                if (r[2][0] == 2) isEmpate = true;                
            }            
        }
    }
    
    public boolean isValidMove(JuegoAcciones action)
    {
        if (action == null || isTerminado || columnas[action.getValue()] >= 6 ) return false;
        return true;
    }

    @Override
    public JuegoEstado getState() {  
        return estado.getCopia();
    }
    
    public int getJugadorActual() {
        return turnoJugador;
    }
    
    public void reiniciar()
    {
        
        turnoJugador = random.nextDouble() < 0.5 ? 0 : 1;
        columnas = new int[JuegoEstado.COLUMNAS];
        isEmpate = false;
        isTerminado = false;
        estado.resetear();
    }
    
    /*
     * Retorna un double r[][] de doubles donde:
     * r[0] = double[5] con la cantidad de 
     *        i fichas adyacentes seguidas, en double[i], del primer jugador
     * r[1] = double[5] con la cantidad de
     *        i fichas adyacentes seguidas, en double[i], del segundo jugador
     * r[2] = double[5] pero con informacion util solo en
     *        double[0], que sera:
     *        -1. si el juego no ha terminado     
     *         0. si el juego ha terminado y jugador 0 gana
     *         1. si el juego ha terminado y jugador 1 gana
     *         2. si el juego ha terminado y es empate
     */
    public static double[][] evaluar(JuegoEstado S)
    {
        double[][] r = new double[3][6];
        boolean end_game = false;
        boolean empate = true;
        int ganador = -1;
	int empty = 0;	                        
	
       
        
	for (int f = 0; f < JuegoEstado.FILAS; f++) {				
	    for (int c = 0; c < JuegoEstado.COLUMNAS; c++) {	    	
	        int x = S.matriz[f][c];	        
	        
                int hor = 1;
                int ver = 1;
                int diag = 1;
                int diag_2 = 1;
                int i = 1;

                
                boolean limite_hor = false;
                boolean limite_ver = false;
                boolean limite_diag = false;
                boolean limite_diag_2 = false;	         

                
                if (x == VACIO) {
                    empty++;
                    hor = 0;
                    ver = 0;
                    diag = 0;
                    diag_2 = 0;
                    limite_hor = true;                    
                    limite_ver = true;
                    limite_diag = true;
                    limite_diag_2 = true;
                }
                
                int jb_hor = x;
                int jb_ver = x;
                int jb_diag = x;
                int jb_diag_2 = x;

                
                // Limite inicial horizontal
                if (c-1 >= 0) {
                    if (S.matriz[f][c-1] == x) { hor = 0; } // ya fue contado en otra pasada
                    else if (S.matriz[f][c-1] != VACIO) { limite_hor = true; }
                } else { limite_hor = true; }

                // Limite inicial vertical
                if (f-1 >= 0) {
                    if (S.matriz[f-1][c] == x) { ver = 0; } // ya fue contado en otra pasada
                    else if (S.matriz[f-1][c] != VACIO) { limite_ver = true; }
                } else { limite_ver = true; }

                // Limite inicial diagonal derecha
                if (c-1 >= 0 && f-1 >= 0) {
                    if (S.matriz[f-1][c-1] == x) { diag = 0; } // ya fue contado en otra pasada
                    else if (S.matriz[f-1][c-1] != VACIO) { limite_diag = true; }
                } else { limite_diag = true; }

                // Limite inicial diagonal izquierda
                if (c+1 < JuegoEstado.COLUMNAS && f-1 >= 0) {
                      if (S.matriz[f-1][c+1] == x) { diag_2 = 0; } // ya fue contado en otra pasada
                      else if (S.matriz[f-1][c+1] != VACIO) { limite_diag_2 = true; }
                } else { limite_diag_2 = true; }
                
                
                
                while (i <= 3) {

                    int new_row = f + i;
                    int new_col = c + i;
                    int left_col = c - i;

                    if (new_row >= JuegoEstado.FILAS) { 	            	
                        if (!limite_ver) { r[x][ver]++; }                        
                        if (!limite_diag) { r[x][diag]++; }	            	
                        if (!limite_diag_2) { r[x][diag_2]++; }	            	
                        ver = 0; 
                        diag = 0; 
                        diag_2 = 0; 
                        jb_ver = -2;
                        jb_diag = -2;
                        jb_diag_2 = -2;
                    }

                    if (new_col >= JuegoEstado.COLUMNAS) { 
                        if (!limite_hor) { r[x][hor]++; }     	            		
                        if (!limite_diag) { r[x][diag]++; }	            		
                        hor = 0; 
                        diag = 0; 
                        jb_hor = -2;
                        jb_diag = -2;
                    }
                    if (left_col < 0) { 
                        if (!limite_diag_2) { r[x][diag_2]++; }	            		
                        diag_2 = 0;                         
                        jb_diag_2 = -2;
                    }

                    if (hor > 0){
                        if (S.matriz[f][new_col] == x) hor++;
                        else if (S.matriz[f][new_col] == VACIO || !limite_hor) { r[x][hor]++; hor = 0; }
                        else { hor = 0; }
                    }

                    if (ver > 0){
                        if (S.matriz[new_row][c] == x) ver++;
                        else if (S.matriz[new_row][c] == VACIO || !limite_ver) { r[x][ver]++; ver = 0; }
                        else { ver = 0; }
                    }

                    if (diag > 0){
                        if (S.matriz[new_row][new_col] == x) diag++;
                        else if (S.matriz[new_row][new_col] == VACIO || !limite_diag) { r[x][diag]++; diag = 0; }
                        else { diag = 0; }
                    }

                    if (diag_2 > 0){
                        if (S.matriz[new_row][left_col] == x) diag_2++;
                        else if (S.matriz[new_row][left_col] == VACIO || !limite_diag_2) { r[x][diag_2]++; diag_2 = 0; }
                        else { diag_2 = 0; }
                    }
                        
                    
                    
                    if (jb_hor > -2 && S.matriz[f][new_col] != jb_hor)
                    {
                        int v = S.matriz[f][new_col];
                        if (jb_hor != VACIO && v != VACIO) jb_hor = -2;
                        if (v != VACIO) jb_hor = v;
                    }
                    
                    if (jb_ver > -2 && S.matriz[new_row][c] != jb_ver)
                    {
                        int v = S.matriz[new_row][c];
                        if (jb_ver != VACIO && v != VACIO) jb_ver = -2;
                        if (v != VACIO) jb_ver = v;
                    }
                    
                    if (jb_diag > -2 && S.matriz[new_row][new_col] != jb_diag)
                    {
                        int v = S.matriz[new_row][new_col];
                        if (jb_diag != VACIO && v != VACIO) jb_diag = -2;
                        if (v != VACIO) jb_diag = v;
                    }
                    
                    if (jb_diag_2 > -2 && S.matriz[new_row][left_col] != jb_diag_2)
                    {
                        int v = S.matriz[new_row][left_col];
                        if (jb_diag_2 != VACIO && v != VACIO) jb_diag_2 = -2;
                        if (v != VACIO) jb_diag_2 = v;
                    }
                    
                    
                    if (i == 3)
                    {
                        if (jb_hor > -2)
                        {
                            if (jb_hor == VACIO) { r[0][5]++; r[1][5]++; }
                            else { r[jb_hor][5]++; }
                        }
                        
                        if (jb_ver > -2)
                        {
                            if (jb_ver == VACIO) { r[0][5]++; r[1][5]++; }
                            else { r[jb_ver][5]++; }
                        }
                        
                        if (jb_diag > -2)
                        {
                            if (jb_diag == VACIO) { r[0][5]++; r[1][5]++; }
                            else { r[jb_diag][5]++; }
                        }
                        
                        if (jb_diag_2 > -2)
                        {
                            if (jb_diag_2 == VACIO) { r[0][5]++; r[1][5]++; }
                            else { r[jb_diag_2][5]++; }
                        }
                    }
                    
                    if (hor >= 4 || ver >= 4 || diag >= 4 || diag_2 >= 4) {         		
                        
                            r[x][hor]++;
                            r[x][ver]++;
                            r[x][diag]++;
                            r[x][diag_2]++;
                        
                        end_game = true;	       	              
                        empate = false;
                        ganador = x;
                    }

                    i++;
	        } 	        
	    }    
	}
        
	if (empty == 0 && !end_game) {
            end_game = true;
            empate = true;
	}
        
        if (!end_game) r[2][0] = -1;
        else if (empate) r[2][0] = 2;
        else r[2][0] = ganador;
        
       return r; 
    }
}
