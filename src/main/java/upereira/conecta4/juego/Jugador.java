package upereira.conecta4.juego;

public interface Jugador {
    public JuegoAcciones actuar(JuegoEstado estado, double reward);
    public String getNombre();
    public Long getTiempoPromedio();
}
