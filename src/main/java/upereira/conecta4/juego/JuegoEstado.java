package upereira.conecta4.juego;

import java.io.Serializable;
import java.util.Arrays;


public class JuegoEstado implements Serializable {

    public static final int COLUMNAS = 7;
    public static final int FILAS = 6;
    
    public int[][] matriz = new int[FILAS][COLUMNAS]; 
    
    public void resetear()
    {
        for (int f = 0; f < FILAS; f++)
            for (int c = 0; c < COLUMNAS; c++)
                matriz[f][c] = Juego.VACIO;
    }
    
    public JuegoEstado getCopia()
    {
        JuegoEstado r = new JuegoEstado();
        int m[][] = new int[FILAS][COLUMNAS];
        
        for (int f = 0; f < FILAS; f++)
            for (int c = 0; c < COLUMNAS; c++)
                m[f][c] = matriz[f][c];
        
        r.matriz = m;
        return r;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + Arrays.deepHashCode(this.matriz);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JuegoEstado other = (JuegoEstado) obj;
        return true;
    }
    
    @Override
    public String toString()
    {
        String r = "";
        for (int i = 0; i < FILAS; i++)
            for (int j = 0; j < COLUMNAS; j++)
                if (matriz[i][j] == -1) r += "_";
                else if (matriz[i][j] == 0) r += "X";
                else r += "O";
        return r;
    }
}