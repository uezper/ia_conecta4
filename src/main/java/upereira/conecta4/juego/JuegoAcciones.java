package upereira.conecta4.juego;

public enum JuegoAcciones {
    UNO(0), DOS(1), TRES(2), CUATRO(3), CINCO(4), SEIS(5), SIETE(6);
    
    private final int value;
    
    private JuegoAcciones(int value)
    {
        this.value = value;
    }
    
    public int getValue()
    {
        return this.value;
    }
}
