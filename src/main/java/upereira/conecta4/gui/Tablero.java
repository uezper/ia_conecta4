package upereira.conecta4.gui;

import upereira.conecta4.juego.Juego;
import upereira.conecta4.juego.JuegoAcciones;
import upereira.conecta4.juego.JuegoEstado;
import upereira.conecta4.juego.Jugador;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.table.DefaultTableModel;
import upereira.conecta4.algoritmos.AgenteRF;

public class Tablero extends javax.swing.JPanel {

    private static final int WIDTH = 450;
    private static final int HEIGHT = 400;
    
    private static final int DIAMETRO = 30;
    private static final int X0 = 90;
    private static final int Y0 = 120;
        
    public Estadisticas estadisticas;
    public Jugador[] jugadores;
    public ModoJuego modoJuego;
    private Juego juego;
    private Thread ejecucion;
    public boolean started;
    public boolean entrenando;
    public int falling_col;
    public int falling_row;    
    private int shine_col;
    public DefaultTableModel model;
    private boolean actualizarStats;
    
    public JuegoAcciones playerMove;
    
    public static enum ModoJuego {
        IAvsIA,
        HvsH,
        IAvsH
    }
    
    /**
     * Creates new form Tablero
     */
    public Tablero() {
        initComponents();                
        this.jugadores = new Jugador[2];
        this.modoJuego = ModoJuego.IAvsH;
        this.juego = new Juego();
        this.falling_col = -1;
        this.falling_row = 0;
        this.started = false;
        this.playerMove = null;        
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.estadisticas = new Estadisticas();
        this.model = null;
    }
    
    public void iniciarJuego()
    {
        
        if (ejecucion != null)
        {
            ejecucion.interrupt();        
        }
        
        
        juego.reiniciar();        
        ejecucion = new Thread(new Ejecucion(this, juego));        
        ejecucion.start();
        started = true;
        actualizarStats = true;
        
    }
    
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        g.setColor(new Color(0, 0, 0));
        g.fillRect(0, 0, WIDTH, HEIGHT);        
        dibujarTablero(g);
        
    }
    
    private void falling(JuegoEstado estado, Graphics g) {
        if (falling_row >= JuegoEstado.FILAS)
        {
            falling_col = -1;
            return;
        }
        
        if (estado.matriz[falling_row][falling_col] == Juego.VACIO)
        {
            Color red = new Color(1f, 0f, 0f, 0.4f);
            Color green = new Color(0.2f, 1f, 0.2f, 0.4f);
            
            if (juego.getJugadorActual() == 0)
                g.setColor(red);
            else
                g.setColor(green);
            
            g.fillArc(X0 + falling_col * (DIAMETRO + 10), Y0 + falling_row * (DIAMETRO + 10), DIAMETRO, DIAMETRO, 0, 360);
            falling_row++;
        }
        else  {
            falling_col = -1;
        }
    }
    
    private void dibujarTablero(Graphics g)
    {
        JuegoEstado estado = juego.getState();
        
        Color empty = new Color(0.8f, 0.8f, 0.8f, 0.6f);
        Color red = new Color(1f, 0f, 0f, 0.8f);
        Color green = new Color(0.2f, 1f, 0.2f, 0.8f);
        
        Color ligh_red = new Color(1f, 0f, 0f, 0.5f);
        Color ligh_green = new Color(0.2f, 1f, 0.2f, 0.5f);
        
        Color shine_red = new Color(1f, 0f, 0f, 0.7f);
        Color shine_green = new Color(0.2f, 1f, 0.2f, 0.7f);
        
        if (started && !juego.isTerminal() && ((modoJuego == ModoJuego.IAvsH && juego.getJugadorActual() == 0) || modoJuego == ModoJuego.HvsH)) {            
            
            for (int j = 0; j < JuegoEstado.COLUMNAS; j++)
            {
                                
                if (juego.getJugadorActual() == 0) {
                    if (shine_col == j) g.setColor(shine_red);
                    else g.setColor(ligh_red);
                }
                else {
                    if (shine_col ==j) g.setColor(shine_green);
                    else g.setColor(ligh_green);
                }                
                
                
                g.fillArc(X0 + j * (DIAMETRO + 10), Y0 + (-1) * (DIAMETRO + 10), DIAMETRO, DIAMETRO, 0, 360);
                
            }
        } else {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        
        
        if (juego.isTerminal())
        {            
            String[] player = new String[]{"Rojo", "Verde"};
            g.setColor(Color.WHITE);
            if (juego.isEmpate()) g.drawString("Empate!!", X0, Y0 - 50);
            else g.drawString(player[(juego.getJugadorActual() + 1) % 2] + " es el ganador!!", X0, Y0 - 50);
            
            
            
            if (this.entrenando)
            {
                MenuPrincipal.tablaRF = ((AgenteRF)jugadores[1]).getTabla();
            }
            
            // Actualizar Estadisticas
            if (!entrenando && actualizarStats) {
                actualizarStats = false;
                if (modoJuego == ModoJuego.IAvsH || modoJuego == ModoJuego.IAvsIA)
                {
                    int ganador = -1;
                    if (!juego.isEmpate()) ganador = (juego.getJugadorActual() + 1) % 2;
                    
                    String algoritmo_1 = null;
                    String algoritmo_2 = null;                                       
                    Long tiempo_1 = null;
                    Long tiempo_2 = null;
                    Estadisticas.TipoEstadistica tipo1 = null;
                    Estadisticas.TipoEstadistica tipo2 = null;
                    
                    if (jugadores[0] != null) algoritmo_1 = jugadores[0].getNombre();
                    if (jugadores[1] != null) algoritmo_2 = jugadores[1].getNombre();                    
                    if (algoritmo_1 != null) tiempo_1 = jugadores[0].getTiempoPromedio();
                    if (algoritmo_2 != null) tiempo_2 = jugadores[1].getTiempoPromedio();
                    if (algoritmo_1 != null) {
                        if (ganador == -1) tipo1 = Estadisticas.TipoEstadistica.EMPATE;
                        else if (ganador == 0) tipo1 = Estadisticas.TipoEstadistica.VICTORIA;
                        else tipo1 = Estadisticas.TipoEstadistica.DERROTA;
                    }
                    if (algoritmo_2 != null) {
                        if (ganador == -1) tipo2 = Estadisticas.TipoEstadistica.EMPATE;
                        else if (ganador == 1) tipo2 = Estadisticas.TipoEstadistica.VICTORIA;
                        else tipo2 = Estadisticas.TipoEstadistica.DERROTA;
                    }
                    
                    if (algoritmo_1 != null)
                    {
                        estadisticas.addEstadistica(algoritmo_1, tipo1, null);
                        estadisticas.addEstadistica(algoritmo_1, Estadisticas.TipoEstadistica.TIEMPO, tiempo_1);
                    }
                    if (algoritmo_2 != null)
                    {
                        estadisticas.addEstadistica(algoritmo_2, tipo2, null);
                        estadisticas.addEstadistica(algoritmo_2, Estadisticas.TipoEstadistica.TIEMPO, tiempo_2);
                    }
                    
                    
                    
                    actualizarResultados();
                    
                }
                
                
            }
            
        }
        
        for (int i = 0; i < JuegoEstado.FILAS; i++)
        {
            for (int j = 0; j < JuegoEstado.COLUMNAS; j++)
            {   
                if (estado.matriz[i][j] == 0) g.setColor(red);
                else if (estado.matriz[i][j] == 1) g.setColor(green);
                else g.setColor(empty);
                
                g.fillArc(X0 + j * (DIAMETRO + 10), Y0 + i * (DIAMETRO + 10), DIAMETRO, DIAMETRO, 0, 360);
            }
        }
        
        if (falling_col != -1)
            falling(estado, g);
        
        
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
        // TODO add your handling code here:
        if (verifyMouse(evt)) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
        } else
        {
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            shine_col = -1;
        }
        
    }//GEN-LAST:event_formMouseMoved

    
    
    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        // TODO add your handling code here:
        
        if (playerMove == null && verifyMouse(evt)) {
            playerMove = JuegoAcciones.values()[shine_col];
        }        
        
    }//GEN-LAST:event_formMouseClicked
    
    
    
    private boolean verifyMouse(java.awt.event.MouseEvent evt)
    {
        
        if (!started || juego.isTerminal()) return false;
                
        double x = evt.getX();
        double y = evt.getY();
        
        
        if ((modoJuego == ModoJuego.IAvsH && juego.getJugadorActual() == 0) || modoJuego == ModoJuego.HvsH) {            
            
            boolean hand = false;
            for (int j = 0; j < JuegoEstado.COLUMNAS; j++)
            {
                
                double xCentro = (X0 + j * (DIAMETRO + 10) + DIAMETRO/2);
                double yCentro = (Y0 + (-1) * (DIAMETRO + 10) + DIAMETRO/2);
                
                
                if (Math.abs(xCentro - x) <= DIAMETRO/2 && Math.abs(yCentro - y) <= DIAMETRO)
                {
                    hand = true;
                    shine_col = j;
                }
                
            }
            
            return hand;
            
        }
        return false;
    }
    
    
    
    public void actualizarResultados()
    {
        
        while (model.getRowCount() != 0)
            model.removeRow(0);
       
        
        Object[][] o = estadisticas.obtenerEstadisticas();
        for (Object[] row : o)
        {
            model.addRow(row);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
