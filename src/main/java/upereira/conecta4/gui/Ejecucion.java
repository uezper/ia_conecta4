package upereira.conecta4.gui;

import upereira.conecta4.juego.Juego;
import upereira.conecta4.juego.JuegoAcciones;
import upereira.conecta4.juego.Jugador;

public class Ejecucion implements Runnable {
    
    private Tablero board;
    private Juego juego;    
    
    public Ejecucion(Tablero board, Juego juego)
    {        
        this.board = board;
        this.juego = juego;
        board.playerMove = null;
    }
    
    @Override
    public void run() {
        
                
        while (!juego.isTerminal() && !Thread.currentThread().isInterrupted())
        {
            boolean wait = false;
            Jugador actual = board.jugadores[juego.getJugadorActual()];
            JuegoAcciones accion = null;
            
            if (actual != null)  {              
                accion = actual.actuar(juego.getState(), juego.getReward());
                wait = true;
            }
            
            else {
                board.playerMove = null;
                while(true)
                {
                    if (board.playerMove != null)
                    {
                        if (juego.isValidMove(board.playerMove))
                        {
                            accion = board.playerMove;                            
                            break;
                        }
                        board.playerMove = null;
                    }
                    board.repaint();
                }
            }
            
           if (wait) {
               try {
                    Thread.sleep(200);
                } catch (InterruptedException ex) {
                    return;
                }
           }
           
           board.falling_col = accion.getValue();
           board.falling_row = 0;
           while (board.falling_col >= 0)
           {
               board.repaint();
               try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    return;
                }
           }
           juego.transition(accion);
           
           if (juego.isTerminal() && board.entrenando)
           {
                double reward = (juego.getJugadorActual() == 0 || juego.isEmpate() ? 1. : -1.);                                        
                reward *= juego.getReward();                   
                board.jugadores[1].actuar(juego.getState(), reward);
           }
           
           board.repaint();
        }
        
    }
    
    
}
