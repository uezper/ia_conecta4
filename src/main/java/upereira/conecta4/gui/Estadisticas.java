package upereira.conecta4.gui;

import java.util.HashMap;

public class Estadisticas {
    
    private HashMap<String, Valores> estadisticas;
    
    public Estadisticas()
    {
        this.estadisticas = new HashMap<>();
    }
    
    public void addEstadistica(String Algoritmo, TipoEstadistica tipo, Long v)
    {
        Valores valor = null;
        
        if (estadisticas.containsKey(Algoritmo))       
            valor = estadisticas.get(Algoritmo);
        else
            valor = new Valores();
    
        switch(tipo)
        {
            case VICTORIA:
                valor.victorias++;
                valor.juegos++;
                valor.rendimiento = (valor.victorias + valor.empates) / (double)valor.juegos;
                break;
            case EMPATE:
                valor.empates++;
                valor.juegos++;
                valor.rendimiento = (valor.victorias + valor.empates) / (double)valor.juegos;
                break;
            case DERROTA:    
                valor.derrotas++;                
                valor.juegos++;
                valor.rendimiento = (valor.victorias + valor.empates) / (double)valor.juegos;
                break;
            case TIEMPO:
                valor.tiempo += v;
                valor.tiempo /= 2;
                break;               
        }
        
        estadisticas.put(Algoritmo, valor);
    }
    
    private Object[] obtenerEstadistica(String algoritmo)
    {
        Object[] o = new Object[7];
        
        Valores valor = estadisticas.get(algoritmo);
        
        o[0] = algoritmo;
        o[1] = (valor.rendimiento * 100.0) + "%";
        o[2] = valor.tiempo + "ns";
        o[3] = valor.victorias;        
        o[4] = valor.derrotas;
        o[5] = valor.empates;
        o[6] = valor.juegos;
                
        return o;
    }
    
    public Object[][] obtenerEstadisticas()
    {
        Object[][] o = new Object[estadisticas.size()][7];
        
        int i = 0;
        for (String key : estadisticas.keySet())
        {
            o[i++] = obtenerEstadistica(key);
        }
        
        return o;
    }
    
    public static enum TipoEstadistica {
        VICTORIA,
        EMPATE,
        DERROTA,
        TIEMPO
    }
    
    private class Valores {
        public int victorias;
        public int empates;
        public int derrotas;  
        public int juegos;
        public double rendimiento;
        public Long tiempo;
        
        public Valores()
        {
            this.victorias = 0;
            this.derrotas = 0;
            this.empates = 0;
            this.juegos = 0;
            this.rendimiento = 0.;
            this.tiempo = new Long(0);
        }
    }
    
}
