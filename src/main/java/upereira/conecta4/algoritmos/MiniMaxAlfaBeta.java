package upereira.conecta4.algoritmos;

import upereira.conecta4.juego.Juego;
import upereira.conecta4.juego.JuegoAcciones;
import upereira.conecta4.juego.JuegoEstado;
import upereira.conecta4.juego.Jugador;

public class MiniMaxAlfaBeta implements Jugador {
    
    private static final double NEG_INF = -(Double.MAX_VALUE - 10);
    private static final double POS_INF = (Double.MAX_VALUE - 10);
    
    private static final int UNO = 1;
    private static final int DOS = 5;
    private static final int TRES = 30;
    private static final int CUATRO = 500;
    private static final int CINCO = 5;
    
    private boolean usaPoda = false;
    private long tiempoAcumulado;
    private int decisionesTomadas;
    private int resultado;
    private int profundidadMaxima;
    private int jugador;
    private int jugadorContrario;
    
    public MiniMaxAlfaBeta(boolean usaPoda, int profundidadMaxima, int jugador)
    {
        this.usaPoda = usaPoda;
        this.profundidadMaxima = profundidadMaxima;
        this.jugador = jugador;
        this.jugadorContrario = (this.jugador + 1) % 2;
        this.decisionesTomadas = 0;
        this.tiempoAcumulado = 0;
    }
    
    @Override
    public JuegoAcciones actuar(JuegoEstado estado, double reward)
    {
        decisionesTomadas++;
        long temp = System.nanoTime();
        
        JuegoAcciones acciones[] = JuegoAcciones.values();
        
        double maxValue = NEG_INF;                
        int maxAccion = 0;
        
        for (int i = 0; i < acciones.length; i++)
        {
            if (hacerMovimiento(estado, acciones[i], jugador)) {
                double value = minimax(estado, maxValue, POS_INF, false, 1);                        
                if (value > maxValue)
                {
                    maxValue = value;
                    maxAccion = i;
                }                
                deshacerMovimiento(estado, acciones[i]);
            }
            
        }        
        temp = System.nanoTime() - temp;
        tiempoAcumulado += temp;
        return acciones[maxAccion];
    }
      
    
    private double minimax(JuegoEstado estado, double alfa, double beta, boolean isMax, int profundidad)
    {
        double[][] r = Juego.evaluar(estado);
        
        // Nodo terminal o profundidad maxima alcanzada
        if (r[2][0] != -1 || profundidad >= profundidadMaxima)
        {            
            if (r[2][0] == jugador) return 1000000000;
            else if(r[2][0] == jugadorContrario) return -1000000000;
            else return obtenerF(r[jugador]) - obtenerF(r[jugadorContrario]);
            
            
        }
        
        JuegoAcciones acciones[] = JuegoAcciones.values();
        for (int i = 0; i < acciones.length; i++)
        {
            if (hacerMovimiento(estado, acciones[i], isMax ? jugador : jugadorContrario)) {
                if (isMax)
                    alfa = Math.max(alfa, minimax(estado, alfa, beta, !isMax, profundidad+1));
                else 
                    beta = Math.min(beta, minimax(estado, alfa, beta, !isMax, profundidad+1));                

                deshacerMovimiento(estado, acciones[i]);                  
                if (alfa >= beta && usaPoda) {                    
                    break;                
                }                
            }
            
        }
        return isMax ? alfa : beta;
    }
    
    private double obtenerF(double[] cantidades)
    {
        return UNO * cantidades[1] + DOS * cantidades[2] + TRES * cantidades[3] + CUATRO * cantidades[4] + CINCO * cantidades[5];
    }
    
    private boolean hacerMovimiento(JuegoEstado estado, JuegoAcciones accion, int player)
    {
        int columna = accion.getValue();
        
        for (int i = JuegoEstado.FILAS - 1; i >= 0; i--)
        {
            if (estado.matriz[i][columna] == Juego.VACIO)
            {
                estado.matriz[i][columna] = player;
                return true;
            }
        }
        return false;
    }
    
    private void deshacerMovimiento(JuegoEstado estado, JuegoAcciones accion)
    {
        int columna = accion.getValue();
        for (int i = 0; i < JuegoEstado.FILAS; i++)
        {
            if (estado.matriz[i][columna] != Juego.VACIO)
            {
                estado.matriz[i][columna] = Juego.VACIO;
                return;
            }
        }
    }
    
    @Override
    public String getNombre()
    {
        return (usaPoda ? "AlfaBeta" : "Minimax") + " (" + profundidadMaxima + ")";
    }
    
    @Override
    public Long getTiempoPromedio()
    {
        if (decisionesTomadas == 0) return new Long(0);
        return tiempoAcumulado / decisionesTomadas;
    }
}
