package upereira.conecta4.algoritmos;

import upereira.conecta4.juego.Juego;
import upereira.conecta4.juego.JuegoAcciones;
import upereira.conecta4.juego.JuegoEstado;
import upereira.conecta4.juego.Jugador;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;

public class AgenteRF implements Jugador {
    
    private static final double NEG_INF = -(Double.MAX_VALUE - 10);
    private static final double POS_INF = (Double.MAX_VALUE - 10);
    private static final int DEFAULT_PLAYER = 0;
    private static final int DEFAULT_ADVERSARY = 1;
    
    private HashMap<String, Value> V;
        
    private long tiempoAcumulado;
    private int decisionesTomadas;
    private int resultado;
    private final int jugador;
    
    private boolean isTraining;    
    private static double epsilon = 0.7;
    private static double alfa = 0.8;
    private double juegosTerminados;
    private double totalRecompensa;
    private JuegoEstado S;    
    private Random random;
    
    public AgenteRF(int jugador)
    {        
        this.jugador = jugador;
        this.decisionesTomadas = 0;
        this.tiempoAcumulado = 0;
        this.S = null;
        this.isTraining = false;                
        this.V = new HashMap<>();
    }
    
    private void normalizarEstado(JuegoEstado estado)
    {
        for (int i = 0; i < JuegoEstado.FILAS; i++)
            for (int j = 0; j < JuegoEstado.COLUMNAS; j++)
                if (estado.matriz[i][j] != -1)
                    estado.matriz[i][j] = (estado.matriz[i][j] == 0 ? 1 : 0);
    }
    
    public void iniciarEntrenamiento()
    {
        this.isTraining = true;  
        this.totalRecompensa = 0;
        this.juegosTerminados = 0;
        this.random = new Random(System.currentTimeMillis());
    }
    
    public void detenerEntrenamiento()
    {
        this.isTraining = false;        
    }
    
    
    
    public void setEpsilon(double e)
    {
        this.epsilon = e;
    }
    
    public void setAlfa(double a)
    {
        this.alfa = a;
    }
        
    public Double getAlfa()
    {
        return this.alfa;
    }
    
    public Double getEpsilon()
    {
        return this.epsilon;
    }
    
    @Override
    public JuegoAcciones actuar(JuegoEstado estado, double R_)
    {
        decisionesTomadas++;
        long temp = System.nanoTime();
        
        JuegoAcciones acciones[] = JuegoAcciones.values();
       
        if (jugador != DEFAULT_PLAYER) normalizarEstado(estado);
        if (isTraining) {      
            
            // Actualizacion de utilidades
            update(estado.getCopia(), R_);
        
            // Probabilidad de tomar una accion aleatoria                        
            if (epsilon >= random.nextDouble())
                return acciones[random.nextInt(acciones.length)];
            
        }
        
        
        double maxValue = NEG_INF;                
        int maxAccion = 0;
        
                
        for (int i = 0; i < acciones.length; i++)
        {
            if (hacerMovimiento(estado, acciones[i], DEFAULT_PLAYER)) {
                double value = V(estado);                        
                if (value > maxValue)
                {
                    maxValue = value;
                    maxAccion = i;
                }
                deshacerMovimiento(estado, acciones[i]);
            }
            
        }
        
        if (isTraining)
        {
            hacerMovimiento(estado, acciones[maxAccion], DEFAULT_PLAYER);
            S = (R_ == 0 ? estado.getCopia() : null);
        }
        
        temp = System.nanoTime() - temp;
        tiempoAcumulado += temp;
        return acciones[maxAccion];
    }
    
    public int getJugador()
    {
        return jugador;
    }
    
    public void reiniciarEstadisticas()
    {
        this.decisionesTomadas = 0;
        this.tiempoAcumulado = 0;
    }
    
    public void update(JuegoEstado S_, double R_)
    {        
        if (S != null) {           
           if (R_ != 0 || V.containsKey(S_) || V.containsKey(S))
           {
               Double VS_ = V(S_);                              
               Double VS = V(S);               
               
               if (R_ != 0) {                   
                   totalRecompensa += R_;
                   juegosTerminados++;
                                 
                   VS_ = (R_ > 0 ? 1.0 : 0.);
                   Value v = V.get(S_);
                   if (v == null)
                      v = new Value();
                   v.utilidad = VS_;
                   V.put(S_.toString(), v);
               }
               
               Value v = V.get(S);               
               if (v == null)
                   v = new Value();                   
                              
               Double error = alfa * (VS_ - VS);                                             
               VS += error;                                  
               
               v.utilidad = VS;               
               V.put(S.toString(), v);
           }
        }    
        
    }
    
    public double V(JuegoEstado S)
    {
        Value r = V.get(S.toString());
        if (r != null) return r.utilidad;
        return 0.5;
    }
    
   
    private boolean hacerMovimiento(JuegoEstado estado, JuegoAcciones accion, int player)
    {
        int columna = accion.getValue();
        
        for (int i = JuegoEstado.FILAS - 1; i >= 0; i--)
        {
            if (estado.matriz[i][columna] == Juego.VACIO)
            {
                estado.matriz[i][columna] = player;
                return true;
            }
        }
        return false;
    }
    
    private void deshacerMovimiento(JuegoEstado estado, JuegoAcciones accion)
    {
        int columna = accion.getValue();
        for (int i = 0; i < JuegoEstado.FILAS; i++)
        {
            if (estado.matriz[i][columna] != Juego.VACIO)
            {
                estado.matriz[i][columna] = Juego.VACIO;
                return;
            }
        }
    }
    
    @Override
    public Long getTiempoPromedio()
    {
        return tiempoAcumulado / decisionesTomadas;
    }
    
    public double getRecompensaMedia()
    {
        if (isTraining && juegosTerminados > 0) return totalRecompensa / juegosTerminados;
        return 0;
    }
    
    
    public HashMap<String, Value> getTabla()
    {
        return V;
    }
    
    public void setTabla(HashMap<String, Value> tabla)
    {
        this.V = tabla;
    }
    
    public int getSize()
    {
        return V.size();
    }
    
    
    
    @Override
    public String getNombre()
    {
        return "AgenteRF";
    }
    
    public static class Value implements Serializable {
        public int numeroVisitas;
        public Double utilidad;
    }
    
}
