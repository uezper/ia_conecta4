package upereira.conecta4.algoritmos;

import java.util.Random;
import upereira.conecta4.gui.MenuPrincipal;
import upereira.conecta4.juego.Juego;

public class Entrenar implements Runnable {

    private double minRendimiento;
    private int minProfundidad;
    private int maxProfundidad;
    private AgenteRF jugador;
    
    public Entrenar(AgenteRF jugador, double minRendimiento, int minProfundidad, int maxProfundidad)
    {
        this.jugador = jugador;
        this.minRendimiento = minRendimiento;
        this.minProfundidad = minProfundidad;
        this.maxProfundidad = maxProfundidad;
    }
    
    @Override
    public void run() {
        minProfundidad = Math.max(2, minProfundidad);
        maxProfundidad = Math.min(4, maxProfundidad);
        
        
        int opNumber = (jugador.getJugador() + 1) % 2;
        
        Random r = new Random(System.currentTimeMillis());
        
        Juego juego = new Juego();
        
        System.out.println("Iniciando entrenamiento... activando alfa y epsilon.");
               
        
        double epsilon = 0.0;
        double alfa = 0.0;
        
        
        jugador.iniciarEntrenamiento();
        jugador.setEpsilon(epsilon);
        jugador.setAlfa(alfa);
        
        int victorias = 0;
        int empates = 0;
        int derrotas = 0;        
        
        double x = 0;
        int c = 0;
        
        
        
        while(!MenuPrincipal.detener)
        {            
            
            
            RandomPlayer oponente = new RandomPlayer();
            //MiniMaxAlfaBeta oponente = new MiniMaxAlfaBeta(true, minProfundidad + r.nextInt(maxProfundidad - minProfundidad + 1), opNumber);

            while (!juego.isTerminal())
            {
                if (juego.getJugadorActual() == opNumber)
                    juego.transition(oponente.actuar(juego.getState(), 0.));
                else
                    juego.transition(jugador.actuar(juego.getState(), juego.getReward()));

                if (juego.isTerminal())
                {
                    double reward = (juego.getJugadorActual() == opNumber || juego.isEmpate() ? 1. : -1.);                                        
                    reward *= juego.getReward();                   

                    jugador.actuar(juego.getState(), reward);

                    if (reward == 1) victorias++;
                    else if (reward == 0.5) empates++;
                    else derrotas++;
                }                
            }
            juego.reiniciar();
            c++;

            x = (victorias + empates)/(double)(victorias + empates + derrotas);
            
            
            if (c % 500 == 0) {                           
                System.out.println(x + " " + jugador.getSize());
                //victorias = empates = derrotas = 0;
            }
            /*if (c % 30000 == 0) {
                FileObjectManager fom = new FileObjectManager("tabla_temp.txt");
                try {
                    fom.escribir(jugador.getTabla());
                } catch (IOException ex) {
                    Logger.getLogger(AgenteRF.class.getName()).log(Level.SEVERE, null, ex);
                }
            }*/
            //if (x > 0.1) { jugador.setEpsilon(0.05); jugador.setAlfa(0.1); }
           if (x > minRendimiento && victorias > 100000000) break;
        }        
        double meanReward = jugador.getRecompensaMedia();
        jugador.detenerEntrenamiento();
        
        System.out.println("===========================================================");
        System.out.println("Entrenamiento finalizado... Desactivando alfa y epsilon");
        System.out.println("Rendimiento: " + x);
        System.out.println("Recompensa media: " + meanReward);
        System.out.println("Victorias: " + victorias);
        System.out.println("Empates: " + empates);
        System.out.println("Derrotas: " + derrotas);
        System.out.println("===========================================================");
        
        MenuPrincipal.detener = false;
        MenuPrincipal.entrenando = false;
        MenuPrincipal.tablaRF = jugador.getTabla();
    }
        
}
