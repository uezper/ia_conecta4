package upereira.conecta4.algoritmos;

import java.util.Random;
import upereira.conecta4.juego.Juego;
import upereira.conecta4.juego.JuegoAcciones;
import upereira.conecta4.juego.JuegoEstado;
import upereira.conecta4.juego.Jugador;

public class RandomPlayer implements Jugador {

    Random random;
    
    public RandomPlayer()
    {
        random = new Random(System.currentTimeMillis());
    }
    @Override
    public JuegoAcciones actuar(JuegoEstado estado, double reward) {
        JuegoAcciones[] acciones = JuegoAcciones.values();
        int i = 0;
        boolean continuar = false;
        while (!continuar)
        {
            i = random.nextInt(acciones.length);
            continuar = hacerMovimiento(estado, acciones[i], 0);
        }
        return acciones[i];
    }

    @Override
    public String getNombre() {
        return "RandomPlayer";
    }

    @Override
    public Long getTiempoPromedio() {
        return null;
    }

    private boolean hacerMovimiento(JuegoEstado estado, JuegoAcciones accion, int player)
    {
        int columna = accion.getValue();
        
        for (int i = JuegoEstado.FILAS - 1; i >= 0; i--)
        {
            if (estado.matriz[i][columna] == Juego.VACIO)
            {
                estado.matriz[i][columna] = player;
                return true;
            }
        }
        return false;
    }
}

