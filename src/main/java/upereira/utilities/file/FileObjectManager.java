package upereira.utilities.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileObjectManager {
    
    private File file;
    
    public FileObjectManager(String nombre)
    {
        file = new File(nombre);
    }
    
    public void escribir(Object object) throws IOException
    {
        FileOutputStream fos = new FileOutputStream(file);
        ObjectOutputStream oos = new ObjectOutputStream(fos);        
        oos.writeObject(object);
        oos.close();
    }

    public <T> T leer() throws IOException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(file);
        ObjectInputStream ois = new ObjectInputStream(fis);
        T object = (T)ois.readObject();
        ois.close();
        return object;       
    }
    
}
