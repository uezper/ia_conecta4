package upereira.utilities.ia;

import java.util.ArrayList;

public abstract class World<S, T extends Enum<T>> {
    protected S estado;   
    protected ArrayList<T> actions;  
    
    
    public abstract S getState();
    
    public ArrayList<T> getActions() {
        return actions;
    }
    public abstract boolean isTerminal();
    public abstract double getReward();
    public abstract void transition(T action);
    
}
